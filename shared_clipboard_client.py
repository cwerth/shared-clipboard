"""Client for data transfer"""
import sys
import time
import ssl
from socket import socket, AF_INET
from threading import Thread, Lock
import pyperclip


class DataTransferClient:
    def __init__(self, host, port, server_hostname):
        self.HOST = host
        self.PORT = port
        self.BUFFER_SIZE = 1024 * 1024 * 10  # 10MB
        self.clipboard_lock = Lock()
        self.clipboard_contents = pyperclip.paste()
        self.ssl_context = ssl.create_default_context()
        self.conn = self.ssl_context.wrap_socket(socket(AF_INET), server_hostname=server_hostname)

    def start(self):
        """
        This starts the connection
        """
        self.conn.connect((self.HOST, self.PORT))
        Thread(target=self.handle_receive).start()
        Thread(target=self.handle_clipboard_updates).start()

    def handle_receive(self):
        """
        This handles receiving data from the server
        """
        while True:
            received_data = self.conn.recv(self.BUFFER_SIZE).decode()
            if received_data is not None:
                self.clipboard_lock.acquire()
                pyperclip.copy(received_data)
                self.clipboard_contents = received_data
                self.clipboard_lock.release()

    def handle_clipboard_updates(self):
        """This checks the clipboard every second and updates the server if needed"""
        while True:
            new_contents = pyperclip.paste()
            self.clipboard_lock.acquire()
            have_new_data = new_contents != self.clipboard_contents
            self.clipboard_lock.release()
            if have_new_data:
                self.conn.sendall(new_contents.encode())
                self.clipboard_contents = new_contents
            time.sleep(1)


if __name__ == "__main__":
    client = DataTransferClient(sys.argv[1], int(sys.argv[2]), sys.argv[3])
    client.start()
