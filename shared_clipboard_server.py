"""Server for data transfer"""
import sys
import ssl
from socket import socket, AF_INET, SOCK_STREAM, SOL_SOCKET, SO_REUSEADDR
from threading import Thread, Lock


class DataTransferServer:
    def __init__(self, host, port, ssl_cert, ssl_key):
        """
        Initializes the server values
        :param ssl_cert: Path to the ssl certificate
        :param ssl_key: Path to the ssl key
        """
        self.HOST = host
        self.PORT = port
        self.connection_list_lock = Lock()
        self.connections = list()
        self.BUFFER_SIZE = 1024*1024*10  # 10MB
        self.server = None
        self.ssl_context = ssl.create_default_context(ssl.Purpose.CLIENT_AUTH)
        self.ssl_context.load_cert_chain(certfile=ssl_cert, keyfile=ssl_key)

    def start(self):
        """
        Starts the server.  Will never return
        """
        self.server = socket(AF_INET, SOCK_STREAM)
        self.server.setsockopt(SOL_SOCKET, SO_REUSEADDR, 1)
        self.server.bind((self.HOST, self.PORT))
        self.server.listen(5)
        print("Ready for connections")

        while True:
            conn, addr = self.server.accept()
            encrypted_conn = self.ssl_context.wrap_socket(conn, server_side=True)
            print("{} connected".format(addr))
            self.connection_list_lock.acquire()
            self.connections.append(encrypted_conn)
            self.connection_list_lock.release()
            Thread(target=self.connection_handler, args=(encrypted_conn,)).start()

    def connection_handler(self, conn):
        """
        This handles receiving from a client
        :param conn:  the client connection
        """
        while True:
            data = conn.recv(self.BUFFER_SIZE).decode()
            self.broadcast_to_all(conn, data)

    def broadcast_to_all(self, source, data):
        """
        This transmits the data to all clients
        :param source: The connection that sent the data
        :param data: The data to transmit
        """
        self.connection_list_lock.acquire()
        for conn in self.connections:
            if conn == source:
                continue
            conn.sendall(data.encode())
        self.connection_list_lock.release()


def run_server(host, port, ssl_cert, ssl_key):
    server = DataTransferServer(host, port, ssl_cert, ssl_key)
    server.start()


if __name__ == "__main__":
    run_server(sys.argv[1], int(sys.argv[2]), sys.argv[3], sys.argv[4])
